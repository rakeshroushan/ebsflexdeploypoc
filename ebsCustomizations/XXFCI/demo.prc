CREATE OR REPLACE PACKAGE demo_plsql_pkg
AS
PROCEDURE test_prc(
errbuf OUT VARCHAR2,
RETCODE OUT VARCHAR2);
END;



CREATE OR REPLACE PACKAGE body demo_plsql_pkg
AS
PROCEDURE test_prc(
errbuf OUT VARCHAR2,
RETCODE OUT VARCHAR2)
AS
lv_cnt NUMBER:= 0;
BEGIN
FOR rec_trx IN
(SELECT trx_number FROM ra_customer_trx_all WHERE rownum<10
)
LOOP
fnd_file.put_line (fnd_file.OUTPUT, rec_trx.trx_number);
lv_cnt:= lv_cnt+1;
END LOOP;
fnd_file.put_line (fnd_file.LOG, lv_cnt ||' Records printed in Output file.');
END test_prc ;
END demo_plsql_pkg;
